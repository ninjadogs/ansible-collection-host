# Publish Ansible Collection

This playbook will build a collection, publish it to ansible galaxy and create and push git tags.
The collection to publish must be in the subdirectory `collection/`, for example:

```
- git root dir/
    - collection/
        - galaxy.yml
    - publish/
        - playbook.yml
        - Makefile
```

To publish a collection use the make target `publish-collection` and pass a `version` to it:

```
make version=1.0.0 publish-collection
```
