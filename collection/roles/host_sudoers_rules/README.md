# host_sudoers_rules

This role adds sudoers rules to `/etc/sudoers.d`.
The name should start with a number.
Higher number rules can overwrite lower number rules.
