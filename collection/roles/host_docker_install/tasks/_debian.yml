---
- name: Ensure the distribution docker versions are not installed
  become: true
  ansible.builtin.package:
    name:
      - docker.io
      - docker-compose
      - docker-doc
      - podman-docker
      - containerd
      - runc
    state: absent

- name: Setup docker apt repository
  vars:
    docker_apt_key: /etc/apt/keyrings/docker.gpg
    docker_download_url: https://download.docker.com/linux/debian
  become: true
  block:
    - name: Update apt cache
      ansible.builtin.apt:
        update_cache: true
        cache_valid_time: 3600
      when:
        - not ansible_check_mode
        - not host_docker_install__skip_apt_update

    - name: Install dependencies to add docker repository
      ansible.builtin.package:
        name:
          - ca-certificates
          - curl
          - gnupg
        state: present

    - name: Ensure keyring directory exists
      ansible.builtin.file:
        path: "{{ docker_apt_key | dirname }}"
        state: directory
        mode: "0755"

    - name: Add official docker apt key
      ansible.builtin.apt_key:
        url: "{{ docker_download_url }}/gpg"
        keyring: "{{ docker_apt_key }}"
        state: present

    - name: Get processor architecture
      tags: skip_ansible_lint
      ansible.builtin.shell:
        cmd: dpkg --print-architecture
      register: r_architecture
      changed_when: false

    - name: Add official docker repository
      ansible.builtin.apt_repository:
        state: present
        repo: >
          deb
          [arch={{ r_architecture.stdout }} signed-by={{ docker_apt_key }}]
          {{ docker_download_url }}
          {{ ansible_distribution_release }}
          stable

- name: Install docker from official repository
  become: true
  ansible.builtin.package:
    name:
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-compose-plugin

- name: Install python docker packages
  become: true
  ansible.builtin.package:
    name:
      - python3-docker
    state: present
  when: not host_docker_install__skip_python_packages
