# host_bash_config

This role can be used to update the bash configuration for each user.
This is useful for updating the prompt or setting the option for vi mode.
